﻿using System;
using System.Collections.Generic;
using yonderarchitecturesample.Domain.Common.Exceptions;

namespace yonderarchitecturesample.Domain.Common.Guard
{
    public class ValidationGuard
    {
        private readonly List<Action> actions = new List<Action>();

        private ValidationGuard()
        {
        }

        public static ValidationGuard Create()
        {
            return new ValidationGuard();
        }

        public static T ParseEnum<T>(string enumPropery, string message = "Invalid property.") where T : struct
        {
            T value;

            if (!Enum.TryParse<T>(enumPropery, out value))
            {
                throw new ValidationException(message);
            }

            return value;
        }

        public ValidationGuard FieldNotNull(string field, string fieldName, string message)
        {
            actions.Add(() =>
            {
                if (string.IsNullOrEmpty(field))
                {
                    throw new ValidationException(fieldName, message);
                }
            });

            return this;
        }

        public ValidationGuard IsNotNull(object data, string message)
        {
            actions.Add(() =>
            {
                if (data == null)
                {
                    throw new ValidationException(message);
                }
            });

            return this;
        }

        public void Validate()
        {
            List<ValidationException> exceptions = new List<ValidationException>();
            foreach (var action in this.actions)
            {
                try
                {
                    action();
                }
                catch (ValidationException ex)
                {
                    exceptions.Add(ex);
                }
            }

            if (exceptions.Count == 1)
            {
                throw exceptions[0];
            }
            else if (exceptions.Count > 1)
            {
                throw new ValidationAggregateException(exceptions.ToArray());
            }
        }
    }
}
