﻿using System;

namespace yonderarchitecturesample.Domain.Common.Exceptions
{
    public class ValidationAggregateException : AggregateException
    {
        public Exception[] Exceptions;

        public ValidationAggregateException(params ValidationException[] innerExceptions) : this("Aggregate exceptions", innerExceptions)
        {
        }

        public ValidationAggregateException(string message, params ValidationException[] innerExceptions) : base(message)
        {
            this.Exceptions = innerExceptions;
        }
    }
}
