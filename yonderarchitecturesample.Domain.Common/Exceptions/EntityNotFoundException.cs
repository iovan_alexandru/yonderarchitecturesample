﻿using System;
using System.Runtime.Serialization;

namespace yonderarchitecturesample.Domain.Common.Exceptions
{
    public class EntityNotFoundException : BadRequestException
    {
        public object EntityId { get; private set; }
        public Type EntityType { get; private set; }
        public EntityNotFoundException(object entityId, Type entityType)
            : this(entityId, entityType, null) { }
        public EntityNotFoundException(object entityId, Type entityType, Exception innerException)
            : this(entityId, entityType, "Entity not found", innerException) { }
        public EntityNotFoundException(object entityId, Type entityType, string message, Exception innerException) : base(message, innerException)
        {
            this.EntityId = entityId;
            this.EntityType = entityType;
        }

        protected EntityNotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
