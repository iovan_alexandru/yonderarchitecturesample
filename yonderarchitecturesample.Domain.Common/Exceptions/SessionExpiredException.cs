﻿using System;
using System.Runtime.Serialization;

namespace yonderarchitecturesample.Domain.Common.Exceptions
{
    public class SessionExpiredException : BadRequestException
    {
        public SessionExpiredException(string message) : base(message)
        {
        }

        public SessionExpiredException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected SessionExpiredException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
