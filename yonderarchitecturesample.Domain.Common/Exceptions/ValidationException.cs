﻿using System;

namespace yonderarchitecturesample.Domain.Common.Exceptions
{
    public class ValidationException : BadRequestException
    {
        public string PropertyName { get; private set; }

        public ValidationException(string message) : this(string.Empty, message)
        {
        }

        public ValidationException(string propertyName, string message) : base(message)
        {
            this.PropertyName = propertyName;
        }

        public ValidationException(string message, Exception innerException) : base(message, innerException)
        {
            this.PropertyName = string.Empty;
        }
    }
}
