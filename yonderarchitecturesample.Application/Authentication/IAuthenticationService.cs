﻿namespace yonderarchitecturesample.Application.Authentication
{
    public interface IAuthenticationService
    {
        void Logon(string username, string password, string schema, string host);

        void Logout();
    }
}
