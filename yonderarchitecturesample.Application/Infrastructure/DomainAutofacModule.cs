﻿using Autofac;
using yonderarchitecturesample.Application.Authentication;

namespace yonderarchitecturesample.Application.Infrastructure
{
    public class DomainAutofacModule : Module
    {
        public DomainAutofacModule() { }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AuthenticationService>().As<IAuthenticationService>().InstancePerLifetimeScope();
        }
    }
}
