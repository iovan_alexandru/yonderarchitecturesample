﻿using System;
using System.Linq;
using yonderarchitecturesample.Database.Base;
using yonderarchitecturesample.Database.Contracts.Students;
using yonderarchitecturesample.Database.Infrastructure;

namespace yonderarchitecturesample.Database.Students
{
    public class StudentRepository : BaseRepository<Student>, IStudentRepository
    {
        public StudentRepository(IDbContextProvider contextProvider) : base(contextProvider)
        {
        }
    }
}
