﻿using System;
using Autofac;
using yonderarchitecturesample.Database.Contracts.Common;

namespace yonderarchitecturesample.Database.Infrastructure.UnitOfWork
{
    public class AutofacUnitOfWork : IUnitOfWork
    {
        private bool _disposed;
        private ILifetimeScope _lifetimeScope;

        public AutofacUnitOfWork(ILifetimeScope context)
        {
            _lifetimeScope = context;
        }

        public T GetService<T>()
        {
            return _lifetimeScope.Resolve<T>();
        }

        public void Rollback()
        {
            var dbContext = GetService<IDbContextProvider>().GetContext();
            dbContext.Dispose();
        }

        public void Save()
        {
            var dbContext = GetService<IDbContextProvider>().GetContext();
            dbContext.SaveChanges();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_lifetimeScope != null)
                    {
                        _lifetimeScope.Dispose();
                    }
                }
                _disposed = true;
            }
        }
    }
}
