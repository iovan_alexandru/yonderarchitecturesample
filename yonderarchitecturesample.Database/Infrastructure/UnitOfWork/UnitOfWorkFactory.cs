﻿using Autofac;
using yonderarchitecturesample.Database.Contracts.Common;

namespace yonderarchitecturesample.Database.Infrastructure.UnitOfWork
{
    public class UnitOfWorkFactory : IUoWFactory
    {
        public static readonly object UnitOfWorkLifetimeScopeTag = "UnitOfWork";

        private ILifetimeScope _rootScope;

        public UnitOfWorkFactory(ILifetimeScope container)
        {
            _rootScope = container;
        }

        public IUnitOfWork BeginUnitOfWork()
        {
            var uow = new AutofacUnitOfWork(_rootScope.BeginLifetimeScope(UnitOfWorkLifetimeScopeTag));
            return uow;
        }
    }
}
