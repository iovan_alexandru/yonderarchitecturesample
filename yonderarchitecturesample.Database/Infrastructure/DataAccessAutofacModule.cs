﻿using Autofac;
using yonderarchitecturesample.Database.Contracts.Common;
using yonderarchitecturesample.Database.Contracts.Students;
using yonderarchitecturesample.Database.Infrastructure.UnitOfWork;
using yonderarchitecturesample.Database.Students;

namespace yonderarchitecturesample.Database.Infrastructure
{
    public class DataAccessAutofacModule : Module
    {
        public DataAccessAutofacModule() { }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DBContextProvider>().As<IDbContextProvider>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWorkFactory>().As<IUoWFactory>().InstancePerLifetimeScope();

            builder.RegisterType<StudentRepository>().As<IStudentRepository>().InstancePerLifetimeScope();
        }
    }
}
