﻿using System;
using System.Data.Entity;

namespace yonderarchitecturesample.Database.Infrastructure
{
    public interface IDbContextProvider : IDisposable
    {
        DbSet<T> Set<T>() where T : class;
        DbContext GetContext();
    }
}
