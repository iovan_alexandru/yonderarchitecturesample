﻿using System.Data.Entity;

namespace yonderarchitecturesample.Database.Infrastructure
{
    public class DBContextProvider : IDbContextProvider
    {
        private DbContext _entities;

        public DBContextProvider(EntitiesContext entitiesContext)
        {
            this._entities = entitiesContext;
        }

        public DbSet<T> Set<T>() where T : class
        {
            return this._entities.Set<T>();
        }

        public DbContext GetContext()
        {
            return _entities;
        }

        public void Dispose()
        {
            this._entities.Dispose();
        }
    }
}
