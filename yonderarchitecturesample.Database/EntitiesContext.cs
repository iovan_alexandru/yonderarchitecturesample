﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using yonderarchitecturesample.Database.Students;

namespace yonderarchitecturesample.Database
{
    public class EntitiesContext : DbContext
    {
        static EntitiesContext()
        {
            System.Data.Entity.Database.SetInitializer(new EntityContextInitializer());
        }

        public EntitiesContext(string connectionString) : base(connectionString)
        {
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Course> Courses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
