﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using yonderarchitecturesample.Database.Contracts.Common;
using yonderarchitecturesample.Database.Infrastructure;

namespace yonderarchitecturesample.Database.Base
{
    public class BaseRepository<T> : IRepository<T> where T : class, IEntity
    {
        protected Lazy<DbContext> Context { get; private set; }

        private Lazy<DbSet<T>> _entities;
        protected DbSet<T> Entities { get { return _entities.Value; } }

        public BaseRepository(IDbContextProvider contextProvider)
        {
            Context = new Lazy<DbContext>(()=>
            {
                return contextProvider.GetContext();
            });
             
            _entities = new Lazy<DbSet<T>>(() => Context.Value.Set<T>());
        }

        public IOrderedQueryable<T> GetAll()
        {
            return Entities;
        }

        public IOrderedQueryable<T> GetAllWithNoTracking(params string[] includes)
        {
            DbQuery<T> query = Entities;

            foreach (var include in includes)
            {
                query = query.Include(include);
            }

            return query.AsNoTracking();
        }

        public T GetById(int id)
        {
            return Entities.Find(id);
        }

        private T GetByIdIncludes(DbQuery<T> entities, int id, string[] includes)
        {
            DbQuery<T> query = entities;
            foreach (var include in includes)
            {
                query = query.Include(include);
            }

            return query.Where(e => e.Id == id).FirstOrDefault();
        }

        public T GetById(int id, params string[] includes)
        {
            return GetByIdIncludes(Entities, id, includes);
        }

        public T GetByIdWithNoTracking(int id, params string[] includes)
        {
            return GetByIdIncludes(Entities.AsNoTracking(), id, includes);
        }

        public void Save(T entity)
        {
            var currentState = Context.Value.Entry(entity).State;

            if (currentState == EntityState.Detached)
            {
                Insert(entity);
            }
        }

        public void Delete(int id)
        {
            var entity = GetById(id);

            Delete(entity);
        }

        public void Delete(T entity)
        {
            Entities.Remove(entity);
        }

        public void Insert(T entity)
        {
            Entities.Add(entity);
        }

        public void Update(T entity)
        {
            Context.Value.Entry(entity).State = EntityState.Modified;
        }
    }
}
