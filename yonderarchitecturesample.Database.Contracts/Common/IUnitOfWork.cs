﻿using System;

namespace yonderarchitecturesample.Database.Contracts.Common
{
    public interface IUnitOfWork : IDisposable
    {
        T GetService<T>();
        void Save();
        void Rollback();
    }
}
