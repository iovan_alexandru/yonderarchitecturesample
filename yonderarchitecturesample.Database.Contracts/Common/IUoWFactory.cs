﻿namespace yonderarchitecturesample.Database.Contracts.Common
{
    public interface IUoWFactory
    {
        IUnitOfWork BeginUnitOfWork();
    }
}
