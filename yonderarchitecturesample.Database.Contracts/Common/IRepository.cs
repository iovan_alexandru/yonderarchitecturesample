﻿using System.Linq;

namespace yonderarchitecturesample.Database.Contracts.Common
{
    public interface IRepository<T> where T : IEntity
    {
        IOrderedQueryable<T> GetAll();
        IOrderedQueryable<T> GetAllWithNoTracking(params string[] includes);
        T GetById(int id);
        T GetById(int id, params string[] includes);
        T GetByIdWithNoTracking(int id, params string[] includes);
        void Save(T entity);
        void Delete(int id);
        void Delete(T entity);
        void Insert(T entity);
        void Update(T entity);
    }
}
