﻿namespace yonderarchitecturesample.Database.Contracts.Common
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
