﻿namespace yonderarchitecturesample.Database.Contracts.Common
{
    public class Entity : IEntity
    {
        public virtual int Id { get; set; }
    }
}
