﻿using System;
using System.Collections.Generic;
using yonderarchitecturesample.Database.Contracts.Common;

namespace yonderarchitecturesample.Database.Students
{
    public class Student : Entity
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime EnrollmentDate { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}
