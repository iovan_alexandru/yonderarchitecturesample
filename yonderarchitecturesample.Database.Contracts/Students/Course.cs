﻿using System.ComponentModel.DataAnnotations.Schema;
using yonderarchitecturesample.Database.Contracts.Common;

namespace yonderarchitecturesample.Database.Students
{
    public class Course : Entity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public override int Id { get; set; }
        public string Title { get; set; }
        public int Credits { get; set; }
    }
}
