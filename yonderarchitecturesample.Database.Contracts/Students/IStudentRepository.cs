﻿using yonderarchitecturesample.Database.Contracts.Common;
using yonderarchitecturesample.Database.Students;

namespace yonderarchitecturesample.Database.Contracts.Students
{
    public interface IStudentRepository : IRepository<Student>
    {
    }
}