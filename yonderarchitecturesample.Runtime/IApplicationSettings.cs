﻿namespace yonderarchitecturesample.Runtime
{
    public interface IApplicationSettings
    {
        T Get<T>(string key);
        string GetConnectionString(string host, string schema);
    }
}
