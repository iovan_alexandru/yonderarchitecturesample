﻿using Microsoft.AspNetCore.Mvc;

namespace YonderArchitectureSample.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
