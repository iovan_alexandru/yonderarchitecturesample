﻿/// <binding BeforeBuild='prepare-debug' Clean='clean' ProjectOpened='copy:css:libs, copy:js:libs' />
"use strict";

var gulp = require("gulp");
var consts = require("./gulp/consts.js")();
require("./gulp/htmls.js")();
require("./gulp/styles.js")();
require("./gulp/scripts.js")();

gulp.task('watchers', function () {
    console.log("setting watchers");
    gulp.watch([consts.paths.sass + "**/*.scss", consts.paths.customSass + "**/*.scss"], ["copy:css:custom"]);
    gulp.watch([consts.paths.webroot + "**/*.html"], ["build:html:templates"]);
});

gulp.task("copy:dependecies", ["copy:js:libs", "copy:css:libs"], function () {
    console.log("all dependecies were copied");
});

gulp.task("clean", ["clean:js", "clean:css", "clean:html:templates"]);

gulp.task('prepare-debug', ['copy:css:custom', 'build:html:templates']);

gulp.task("prepare-release", ['copy:dependecies', 'copy:css:custom', 'build:html:templates']); // TO-DO: add js minification