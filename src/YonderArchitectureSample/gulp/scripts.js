﻿'use strict';

var gulp = require("gulp");
var gulpPlugins = require('gulp-load-plugins')();
var consts = require("./consts.js")();
var commonTasks = require("./common.js")();

module.exports = function () {
    gulp.task('copy:js:libs', ['clean:js'], function (cb) {
        console.log("copying js libs");
        var vendorJs = [ ];

        gulp
            .src(vendorJs)
            .pipe(gulpPlugins.concat("vendor.js"))
            .pipe(gulpPlugins.minify({
                ext: {
                    src: '-debug.js',
                    min: '.js'
                }
            }))
            .pipe(gulp.dest(consts.paths.lib));

        //cb();
    });

    gulp.task("clean:js", function (cb) {
        console.log("cleaning js");
        //commonTasks.cleanFiles([consts.lib], cb);
    });
};