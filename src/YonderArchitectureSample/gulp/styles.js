﻿/* jshint mode: true */

'use strict';

var gulp = require("gulp");
var consts = require("./consts.js")();
var commonTasks = require("./common.js")();

var gulpPlugins = require('gulp-load-plugins')();
var cssAutoPrefixed = require("gulp-autoprefixer");
var eventStream = require('event-stream');

module.exports = function () {
    gulp.task("copy:css:custom", function (cb) {
        console.log("compiling custom sass");
        var customSass = gulp.src(consts.paths.customSass + "**/*.scss");
        var fileStream = eventStream.concat(customSass);

        fileStream
            .pipe(gulpPlugins.sass().on('error', gulpPlugins.sass.logError))
            .pipe(cssAutoPrefixed({ browsers: ['last 2 versions', 'not ie <= 8'] }))
            .pipe(gulpPlugins.concat("custom.css"))
            .pipe(gulpPlugins.cssmin())
            .pipe(gulp.dest(consts.paths.css));
        cb();
    });

    gulp.task('copy:css:libs', ['clean:css'], function () {
        console.log("copying css dependecies");
        var vendorCssFiles = [ ];

        var vendorSassFiles = [
            consts.paths.bower + "bootstrap-daterangepicker/daterangepicker.scss",
            consts.paths.bower + "angularjs-toaster/toaster.scss"
        ];

        var compiledCssLibs = gulp
            .src(vendorSassFiles)
            .pipe(gulpPlugins.sass().on('error', gulpPlugins.sass.logError));

        var fileStream = eventStream.concat(gulp.src(vendorCssFiles), compiledCssLibs);

        return fileStream
            .pipe(gulpPlugins.concat("vendor.css"))
            .pipe(gulpPlugins.cssmin())
            .pipe(gulp.dest(consts.paths.css));
    });

    gulp.task("clean:css", function (cb) {
        console.log("cleaning css");
        commonTasks.cleanFiles([consts.paths.css], cb);
    });
};