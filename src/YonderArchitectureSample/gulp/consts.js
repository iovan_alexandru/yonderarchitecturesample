﻿/* jshint mode: true */

'use strict';

module.exports = function () {
    var consts = {};
    consts.paths = {
        webroot: "./wwwroot/",
        bower: "./bower_components/",
        lib: "./wwwroot/lib/",
        css: "./wwwroot/css/",
        customSass: "./wwwroot/app/",
        fonts: "./wwwroot/fonts/",
    };

    return consts;
};