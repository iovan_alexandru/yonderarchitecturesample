﻿'use strict';

var gulp = require("gulp");
var del = require("del");

module.exports = function () {
    var mInterface = {};

    mInterface.copyFiles = function (sourceDir, destinationDir, fileDictionary) {
        for (var file in fileDictionary) {
            gulp.src(sourceDir + fileDictionary[file]).pipe(gulp.dest(destinationDir));
        }
    };

    mInterface.cleanFiles = function (pathsToDelete, doneFn) {
        del(pathsToDelete).then(function () {
            if (doneFn != undefined) {
                doneFn();
            }
        });
    };

    return mInterface;
};