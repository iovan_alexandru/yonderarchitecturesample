﻿/* jshint mode: true */

'use strict';

var gulp = require("gulp");
var minifyHtml = require("gulp-htmlmin");
var templateCache = require("gulp-angular-templatecache");
var consts = require("./consts.js")();
var commonTasks = require("./common.js")();

module.exports = function () {
    var templateConsts = {
    };

    gulp.task("build:html:templates", function () {
        console.log("creating template caches...");
        // unifyTemplates(templateConsts.coreHtmlsSrc, templateConsts.coreHtmlsDest, "Connect.core", "tmpl.min.js", templateConsts.corePrefix);
    });

    gulp.task("clean:html:templates", function () {
        //commonTasks.cleanFiles([
        //    templateConsts.coreHtmlsDest + "tmpl.min.js"
        //]);
    });

    function unifyTemplates(filesRegex, destDir, module, fileName, rootPrefix) {
        return gulp.src(filesRegex)
        .pipe(minifyHtml({ empty: true, quotes: true }))
        .pipe(templateCache(fileName, {
            standalone: false,
            module: module,
            root: rootPrefix
        }))
        .pipe(gulp.dest(destDir));
    };
};