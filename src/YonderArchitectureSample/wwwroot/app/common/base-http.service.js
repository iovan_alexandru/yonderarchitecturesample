﻿'use strict';

yonder.architectureSample.service('BaseHttpService', BaseHttpService);

BaseHttpService.$inject = ['$q', '$http', '$state'];

function BaseHttpService($q, $http, $state) {
    var service = this;
    service.STATUS_CODES = {
        BadRequest: 400,
        Unauthorized: 401,
        Forbidden: 403,
        ServerError: 500
    };

    function getApiUrl() {
        return undefined; // change this
    }

    // This function can be used to alter promise results.
    // It returns a promise as a result.
    service.pipe = function (promise, middlewareFn) {
        var defered = $q.defer();

        promise.then(function (response) {
            defered.resolve(middlewareFn(response));
        }).catch(function (response) {
            defered.reject(response);
        });

        return defered.promise;
    };

    // This function can be used to filter promise results.
    // It returns a promise as a result.
    service.pipeFilter = function (promise, itemFilterFn) {
        var defered = $q.defer();

        promise.then(function (response) {
            if (response != undefined && angular.isArray(response)) {
                var filteredItems = response.filter(itemFilterFn);
                defered.resolve(filteredItems);
                return;
            }

            // Filter could not be done.
            defered.resolve(response);
        }).catch(function (response) {
            defered.reject(response);
        });

        return defered.promise;
    };

    service.makeGet = function (url, data) {
        var params = angular.copy(data);
        addRandomProperty(params);

        return makeHttpRequest({
            method: 'GET',
            url: getApiUrl() + appendRandom(url),
            withCredentials: true
        },
        params,
        undefined);
    };

    service.makePost = function (url, data) {
        return makeHttpRequest({
            method: 'POST',
            url: getApiUrl() + url,
            withCredentials: true
        },
        undefined,
        data);
    };

    service.makePut = function (url, data) {
        return makeHttpRequest({
            method: 'PUT',
            url: getApiUrl() + url,
            withCredentials: true
        },
        undefined,
        data);
    };

    service.makeDelete = function (url, data) {
        var params = angular.copy(data);
        addRandomProperty(params);
        return makeHttpRequest({
            method: 'DELETE',
            url: getApiUrl() + url,
            withCredentials: true
        },
        params,
        undefined);
    };

    function makeHttpRequest(options, params, data) {
        var defered = $q.defer();

        if (params != undefined) {
            var processedParams = angular.copy(params);
            options.params = processedParams;
        }

        if (data != undefined) {
            var processedData = angular.copy(data);
            options.data = processedData;
        }

        $http(options).success(function (response, status, headers) {
            // Decrease the dates with the timezone.
            defered.resolve(response);
        }).error(function (response, status, headers) {
            handleServerErrorResponse(defered, status, response);
        });

        return defered.promise;
    }

    function handleServerErrorResponse(defered, status, response) {
        if (status == service.STATUS_CODES.Unauthorized) {
            redirectToLogin();
            return;
        }

        var responseWrapper = {
            errors: response,
            status: status,
            statusMessage: parseResponseCodes(status)
        };

        if (status == service.STATUS_CODES.Forbidden) {
            responseWrapper.displayMessage = buildErrorMessage(response);
        }

        if (status == service.STATUS_CODES.BadRequest) {
            var displayMessage = buildErrorMessage(response);
            if (displayMessage) {
                responseWrapper.displayMessage = displayMessage;
            }
        }

        defered.reject(responseWrapper);
    }



    function buildErrorMessage(response) {
        var message = undefined;
        if (angular.isArray(response)) {
            var msgs = [];
            for (var i = 0; i < response.length; i++) {
                if (angular.isString(response[i].message)) {
                    msgs.push(response[i].message);
                }
            }

            if (msgs.length > 0) {
                message = msgs.join(' ');
            }
        } else if (angular.isObject(response) && angular.isString(response.message)) {
            message = response.message;
        }

        return message;
    }

    function addRandomProperty(data) {
        if (data == undefined) {
            data = {};
        }

        data.rand = Math.floor(Math.random() * 100000);
    }

    function parseResponseCodes(status) {
        switch (status) {
            case -1:
                return 'Service unreacheable';
            case service.STATUS_CODES.BadRequest:
                return 'Bad request';
            case service.STATUS_CODES.Unauthorized:
                return 'Unauthorized request';
            case service.STATUS_CODES.Forbidden:
                return 'Forbidden request';
        }

        if (status >= 500) {
            return 'Server error';
        }

        return '';
    }

    function appendRandom(url) {
        if (url == undefined) {
            return;
        }

        var prefix = "";
        if (url.indexOf('?') == -1) {
            prefix = "?";
        }

        var rand = Math.floor(Math.random() * 100000);
        return url + prefix + "&rand=" + rand;
    }

    function redirectToLogin() {
        $state.go("login");
    }
}
