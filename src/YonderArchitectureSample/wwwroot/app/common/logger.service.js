﻿yonder.architectureSample.service('Logger', Logger);

function Logger() {
    var service = this;

    function formatMessage(tag, msg, date) {
        var str = ''
        str += date + ' | ';
        if (tag != undefined) {
            str += tag + ' - ';
        }
        str += msg;

        return str;
    }

    service.error = function (error, tag) {
        console.error(formatMessage(tag, error, new Date()));
    };

    service.info = function (info, tag) {
        console.info(formatMessage(tag, info, new Date()));
    };

    service.warning = function (warn, tag) {
        console.warn(formatMessage(tag, warn, new Date()));
    };
}