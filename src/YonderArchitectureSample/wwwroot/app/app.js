﻿var yonder = {};
/*
* Contains all components that can be shared between.
*/
yonder.architectureSample = angular.module("yonder.architectureSample", ['ui.router', 'ngSanitize', 'ui.bootstrap']);

yonder.architectureSample.config(routerConfig);
yonder.architectureSample.constant("CONSTANTS", {
    // ADD CONSTANTS HERE
});

routerConfig.$inject = ['$stateProvider', '$urlRouterProvider'];

function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'loginCtrl',
        overwritesRedirect: true
    });

    $urlRouterProvider.otherwise('/login');
}