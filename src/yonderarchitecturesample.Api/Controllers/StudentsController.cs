﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using yonderarchitecturesample.Api.Dtos;
using yonderarchitecturesample.Api.Dtos.Mappers;
using yonderarchitecturesample.Database.Contracts.Common;
using yonderarchitecturesample.Database.Contracts.Students;
using yonderarchitecturesample.Database.Students;
using yonderarchitecturesample.Domain.Common.Guard;

namespace yonderarchitecturesample.Api.Controllers
{
    [Route("api/[controller]")]
    public class StudentsController : Controller
    {
        protected IStudentRepository StudentRepository { get; set; }
        public IUoWFactory UowFactory { get; private set; }

        public StudentsController(IUoWFactory uowFactory, IStudentRepository studentRepository)
        {
            UowFactory = uowFactory;
            StudentRepository = studentRepository;
        }

        [HttpGet]
        public IEnumerable<StudentDto> Get()
        {
            var students = StudentRepository
                .GetAllWithNoTracking()
                .Select(StudentDtoMapper.Map)
                .ToList();

            return students;
        }

        [HttpGet]
        [Route("{id}")]
        public StudentDto Get(int id)
        {
            var student = StudentRepository
                .GetByIdWithNoTracking(id);
            StudentRepository.GetAllWithNoTracking().Where(s => s.Id == id).FirstOrDefault();

            return StudentDtoMapper.Map(student);
        }

        [HttpPost]
        public StudentDto Post([FromBody]StudentDto student)
        {
            ValidationGuard.Create()
                .IsNotNull(student, "Invalid data")
                .FieldNotNull(student?.FirstName, "FirstName", "First name is mandatory")
                .FieldNotNull(student?.LastName, "LastName", "Last name is mandatory")
                // .FieldNotNull(student?.EnrollmentDate, "EnrollmentDate", "Enrollment date is mandatory")
                .Validate();

            using (var uow = UowFactory.BeginUnitOfWork())
            {
                var studentRepository = uow.GetService<IStudentRepository>();
                var studentDb = new Student()
                {
                    FirstName = student.FirstName,
                    LastName = student.LastName,
                    EnrollmentDate = student.EnrollmentDate.Value
                };

                studentRepository.Insert(studentDb);
                uow.Save();

                return StudentDtoMapper.Map(studentDb);
            }
        }
    }
}
