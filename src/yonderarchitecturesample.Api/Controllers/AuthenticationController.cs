﻿using Microsoft.AspNetCore.Mvc;
using yonderarchitecturesample.Application.Authentication;

namespace yonderarchitecturesample.Api.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        protected IAuthenticationService AuthenticationService { get; private set; }

        public AuthenticationController(IAuthenticationService authService)
        {
            AuthenticationService = authService;
        }

        [HttpGet("login")]
        public bool Login()
        {
            return false;
        }
    }
}
