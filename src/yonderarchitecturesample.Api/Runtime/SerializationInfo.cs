﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace yonderarchitecturesample.Api.Runtime
{
    public static class SerializationInfo
    {
        public static JsonSerializerSettings SerializationSettings = new JsonSerializerSettings()
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Formatting = Formatting.Indented
        };

        public static void ApplyJsonSettings(MvcJsonOptions settings)
        {
            settings.SerializerSettings.ContractResolver = SerializationSettings.ContractResolver;
            settings.SerializerSettings.Formatting = SerializationSettings.Formatting;
        }
    }
}
