﻿using yonderarchitecturesample.Database.Students;

namespace yonderarchitecturesample.Api.Dtos.Mappers
{
    public static class StudentDtoMapper
    {
        public static StudentDto Map(Student student)
        {
            if(student == null)
            {
                return null;
            }

            return new StudentDto()
            {
                Id = student.Id,
                FirstName = student.FirstName,
                LastName = student.LastName,
                EnrollmentDate = student.EnrollmentDate
            };
        }
    }
}
