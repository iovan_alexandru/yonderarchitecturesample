﻿using System;

namespace yonderarchitecturesample.Api.Dtos
{
    public class StudentDto
    {
        public DateTime? EnrollmentDate { get; set; }
        public string FirstName { get; set; }
        public int Id { get; set; }
        public string LastName { get; set; }
    }
}
