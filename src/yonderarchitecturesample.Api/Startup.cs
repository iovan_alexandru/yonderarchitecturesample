﻿using System;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using yonderarchitecturesample.Api.Middlewares;
using yonderarchitecturesample.Api.Runtime;
using yonderarchitecturesample.Application.Infrastructure;
using yonderarchitecturesample.Database;
using yonderarchitecturesample.Database.Infrastructure;
using yonderarchitecturesample.Runtime.Security;

namespace yonderarchitecturesample.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
            {
                options.SignInScheme = SecurityOptions.AuthenticationSchema;
            });

            // Add framework services.
            services.AddMvc().AddJsonOptions(SerializationInfo.ApplyJsonSettings);
            services.AddCors(corsConfig =>
            {
                var policy = new CorsPolicy();
                policy.Headers.Add("*");
                policy.Methods.Add("*");
                policy.Origins.Add("*");
                policy.SupportsCredentials = true;

                corsConfig.AddPolicy("CICors", policy);
            });

            services.AddScoped(provider =>
            {
                var connectionString = Configuration["Data:EntityDb:ConnectionString"];
                return new EntitiesContext(connectionString);
            });

            // Create the Autofac container builder.
            var builder = new ContainerBuilder();

            // Add any Autofac modules or registrations.
            builder.RegisterModule(new DataAccessAutofacModule());
            builder.RegisterModule(new DomainAutofacModule());

            // Populate the services.
            builder.Populate(services);

            // Build the container.
            var container = builder.Build();

            // Resolve and return the service provider.
            return container.Resolve<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseDeveloperExceptionPage();
            app.UseMiddleware<ExceptionHandlerMiddleware>();

            //app.UseIISPlatformHandler();

            app.UseStaticFiles();

            app.UseCookieAuthentication(new CookieAuthenticationOptions()
            {
                AuthenticationScheme = SecurityOptions.AuthenticationSchema,
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                SlidingExpiration = true
            });

            app.UseCors("CICors");

            app.UseMvc();
        }
    }
}
