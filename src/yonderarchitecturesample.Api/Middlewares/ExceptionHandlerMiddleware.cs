﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using yonderarchitecturesample.Api.Runtime;
using yonderarchitecturesample.Domain.Common.Exceptions;

namespace yonderarchitecturesample.Api.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate next;
        private const string UNAUTHORIZED_MESSAGE = "Unauthorized request.";
        private const string FORBIDDEN_MESSAGE = "Forbidden request: ";

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await this.next.Invoke(context);
            }
            catch (ValidationException ex)
            {
                ChangeResponse(context, ex);
            }
            catch (ValidationAggregateException ex)
            {
                ChangeResponse(context, ex);
            }
            catch (SessionExpiredException)
            {
                UnauthorizedAccessResponse(context);
            }
            catch (ForbiddenAccessException ex)
            {
                ForbiddenAccesResponse(context, ex);
            }
            catch (EntityNotFoundException ex)
            {
                ChangeResponse(context, ex);
            }
            catch (BadRequestException ex)
            {
                ChangeResponse(context, ex);
            }
        }

        private void UnauthorizedAccessResponse(HttpContext context)
        {
            SetResponseDetails(context, StatusCodes.Status401Unauthorized, UNAUTHORIZED_MESSAGE);
        }

        private void ForbiddenAccesResponse(HttpContext context, ForbiddenAccessException ex)
        {
            SetResponseDetails(context, StatusCodes.Status403Forbidden, FORBIDDEN_MESSAGE + ex.Message);
        }

        private void SetResponseDetails(HttpContext context, int statusCode, string message)
        {
            context.Response.StatusCode = statusCode;
            var bytes = Encoding.UTF8.GetBytes(message);
            context.Response.Body.Write(bytes, 0, bytes.Length);
        }

        private void ChangeResponse(HttpContext context, BadRequestException ex)
        {
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            WriteObjectToBody(context, new
            {
                Message = ex.Message
            });
        }

        private void ChangeResponse(HttpContext context, EntityNotFoundException ex)
        {
            context.Response.StatusCode = StatusCodes.Status404NotFound;
            WriteObjectToBody(context, BuildEntityNotFoundExceptionResponseObject(ex));
        }

        private void WriteObjectToBody(HttpContext context, object data)
        {
            var responseString = JsonConvert.SerializeObject(data, SerializationInfo.SerializationSettings);
            var bytes = Encoding.UTF8.GetBytes(responseString);
            context.Response.Body.Write(bytes, 0, bytes.Length);
        }

        private void ChangeResponse(HttpContext context, Exception ex)
        {
            dynamic responseObject;
            if (ex is ValidationException)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                responseObject = BuildValidationExceptionResponseObject((ValidationException)ex);
            }
            else if (ex is ValidationAggregateException)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                responseObject = BuildValidationExceptionResponseObject((ValidationAggregateException)ex);
            }
            else
            {
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                responseObject = BuildValidationExceptionResponseObject(ex);
            }

            WriteObjectToBody(context, responseObject);
        }

        private dynamic BuildValidationExceptionResponseObject(Exception ex)
        {
            return new
            {
                Message = ex.Message,
                Type = ex.GetType().Name
            };
        }

        private dynamic BuildValidationExceptionResponseObject(ValidationException ex)
        {
            return new
            {
                Message = ex.Message,
                Type = ex.GetType().Name,
                PropertyName = ex.PropertyName
            };
        }

        private dynamic BuildValidationExceptionResponseObject(ValidationAggregateException ex)
        {
            dynamic baseResponse = new
            {
                Message = ex.Message,
                Type = ex.GetType().Name,
                Exceptions = new List<dynamic>()
            };

            foreach (var exception in ex.Exceptions)
            {
                baseResponse.Exceptions.Add(BuildValidationExceptionResponseObject((ValidationException)exception));
            }

            return baseResponse;
        }

        private dynamic BuildEntityNotFoundExceptionResponseObject(EntityNotFoundException ex)
        {
            return new
            {
                EntityId = ex.EntityId,
                EntityType = ex.EntityType.FullName,
                Message = ex.Message == null ? "Entity not found" : ex.Message
            };
        }
    }
}
